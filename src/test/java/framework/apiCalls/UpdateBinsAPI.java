package framework.apiCalls;

import framework.utils.ConfigurationReader;
import io.restassured.response.Response;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class UpdateBinsAPI extends BaseCalls {

    public Response updateBins(String path, Map<String, String> updateAPI) {
        String binType = updateAPI.get("privateBin");
        String secretKey = "";

        if (binType.equals("true")) {
            secretKey = ConfigurationReader.get("secretKey");
        } else if (binType.equals("invalid")) {
            secretKey = "invalid";
        }

        response = given()
                .spec(SetBaseUri())
                .contentType(updateAPI.get("contentType"))
                .header("secret-key", secretKey)
                .header("versioning", updateAPI.get("versioning"))
                .body("{\n" +
                        "    \"sample\": \"Hello World\"\n" +
                        "}")
                .put(path);

        return response;
    }

    public Response updateBinsWithoutBody(String path, Map<String, String> updateAPI) {
        String secretKey = ConfigurationReader.get("secretKey");

        response = given()
                .spec(SetBaseUri())
                .contentType(updateAPI.get("contentType"))
                .header("secret-key", secretKey)
                .header("versioning", updateAPI.get("versioning"))
                .body("")
                .put(path);

        return response;
    }
}
