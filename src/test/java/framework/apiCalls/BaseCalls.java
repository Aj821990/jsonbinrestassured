package framework.apiCalls;

import framework.utils.ConfigurationReader;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;

/**
 * This is the base class where common methods are declared
 */

public class BaseCalls {

    Response response = null;
    JSONObject body = new JSONObject();

    public static RequestSpecification SetBaseUri()
    {
        RequestSpecification requestSpec = new RequestSpecBuilder().build();
        requestSpec.baseUri(ConfigurationReader.get("baseURI"));

        return requestSpec;
    }
}
