package framework.apiCalls;

import framework.utils.ConfigurationReader;
import io.restassured.response.Response;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class DeleteBinsAPI extends BaseCalls {

    public Response deleteBins(String path, Map<String, String> deleteAPI)
    {
        String binType = deleteAPI.get("privateBin");
        String secretKey = "";

        if (binType.equals("true")) {
            secretKey = ConfigurationReader.get("secretKey");
        } else if (binType.equals("invalid")) {
            secretKey = "invalid";
        }

        response = given()
                .spec(SetBaseUri())
                .header("secret-key", secretKey)
                .delete(path);

        return response;
    }
}
