package framework.apiCalls;

import framework.utils.ConfigurationReader;
import io.restassured.response.Response;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class ReadBinsAPI extends BaseCalls {
    public Response getBins(String path, Map<String,String> privateBin)
    {
        String binType = privateBin.get("privateBin");
        String secretKey = "";

        if(binType.equals("true"))
        {
            secretKey = ConfigurationReader.get("secretKey");
        } else if(binType.equals("invalid"))
        {
            secretKey = "invalid";
        }

        response = given()
                .spec(SetBaseUri())
                .header("secret-key", secretKey)
                .get(path);

        return response;
    }
}
