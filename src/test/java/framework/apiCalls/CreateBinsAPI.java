package framework.apiCalls;

import framework.utils.ConfigurationReader;
import io.restassured.response.Response;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class CreateBinsAPI extends BaseCalls {

    String secretKey = null;

    public Response createBins(String path, Map<String,String> createAPI) {

        String secretKeyCondition = createAPI.get("secretKey");
        String collectionID = "";
        String collectionName = createAPI.get("collectionName");
        secretKey = ConfigurationReader.get("secretKey");

        if(!collectionName.isEmpty() && !collectionName.equals("invalid"))
        {
            body.clear();
            body.put("name", collectionName);

            response = given()
                    .spec(SetBaseUri())
                    .header("Content-Type", "application/json")
                    .header("secret-key", secretKey)
                    .body(body)
                    .post("/c")
                    .then()
                    .extract()
                    .response();

            collectionID = response.path("id");
        }

        if(secretKeyCondition.equals("invalid")) {
            secretKey = "invalid";
        } else if(secretKeyCondition.equals("blank")) {
            secretKey = "";
        }

        if(collectionName.equals("invalid")) {
            collectionID = "invalid";
        } else if(collectionName.equals("blank")) {
            collectionID = "";
        }

        response = given()
                .spec(SetBaseUri())
                .contentType(createAPI.get("contentType"))
                .header("secret-key", secretKey)
                .header("collection-id", collectionID)
                .header("private", createAPI.get("private"))
                .header("name", createAPI.get("binName"))
                .body("{\n" +
                        "    \"sample\": \"Hello World\"\n" +
                        "}")
                .post(path);

        return response;
    }

    public Response createBinsWithoutOptionalHeaders(String path, Map<String,String> createAPI)
    {
        secretKey = ConfigurationReader.get("secretKey");
        response = given()
                .spec(SetBaseUri())
                .header("Content-Type", createAPI.get("contentType"))
                .header("secret-key", secretKey)
                .body("{\n" +
                        "    \"sample\": \"Hello World\"\n" +
                        "}")
                .post(path);

        return response;
    }

    public Response createBinsWithoutBody(String path, Map<String,String> createAPI)
    {
        secretKey = ConfigurationReader.get("secretKey");
        response = given()
                .spec(SetBaseUri())
                .header("Content-Type", createAPI.get("contentType"))
                .header("secret-key", secretKey)
                .body("")
                .post(path);

        return response;
    }

}
