package framework.step_definitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.apiCalls.CreateBinsAPI;
import framework.apiCalls.DeleteBinsAPI;
import framework.apiCalls.ReadBinsAPI;
import framework.apiCalls.UpdateBinsAPI;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

/**
 * This is the common step definition file for all the feature files.
 *
 */

public class BaseStepDef {
    RequestSpecification requestSpecification;
    Response response;
    String fullPath;
    static Map<String, String> binids= new HashMap<>();

    CreateBinsAPI createBinsAPI = new CreateBinsAPI();
    ReadBinsAPI readBinsAPI = new ReadBinsAPI();
    UpdateBinsAPI updateBinsAPI = new UpdateBinsAPI();
    DeleteBinsAPI deleteBinsAPI = new DeleteBinsAPI();

    @When("User sends POST request to create bin using route {string}")
    public void userSendsPOSTRequestToCreateBinUsingRoute(String path, Map<String,String> createAPI) {
        response = createBinsAPI.createBins(path, createAPI);
    }

    @When("User sends POST request to create bin using only mandatory headers route {string}")
    public void userSendsPOSTRequestToCreateBinUsingOnlyMandatoryHeadersRoute(String path, Map<String,String> createAPI) {
        response = createBinsAPI.createBinsWithoutOptionalHeaders(path, createAPI);
    }

    @When("User sends POST request to create bin using without body route {string}")
    public void userSendsPOSTRequestToCreateBinUsingWithoutBodyRoute(String path, Map<String,String> createAPI) {
        response = createBinsAPI.createBinsWithoutBody(path, createAPI);
    }

    @Then("Verify that response status code is {}")
    public void verifyThatResponseStatusCodeIsStatusCode(int statusCode) {
        assertEquals(statusCode, response.statusCode());
    }

    @And("Verify the response schema is {string}")
    public void verifyTheResponseSchemaIs(String schemaPath) {
        String responseBody = response.getBody().asString();
        assertThat(responseBody,matchesJsonSchemaInClasspath(schemaPath));
    }

    @And("Verify that response content type is {string}")
    public void verifyThatResponseContentTypeIs(String contentType) {
        Assert.assertTrue(response.contentType().contains(contentType));
    }

    @And("Verify the response header contains {string}")
    public void verifyTheResponseHeaderContains(String header) {
        Assert.assertTrue(response.getHeaders().hasHeaderWithName(header));
    }

    @And("Verify the response body has success {string}")
    public void verifyTheResponseBodyHasSuccess(String value) {
        String successValue = response.getBody().jsonPath().getString("success");
        Assert.assertEquals(value, successValue);
    }

    @And("Verify the response body has tag {string}")
    public void verifyTheResponseBodyHasTag(String tag) {
        Assert.assertTrue(response.getBody().asString().contains(tag));
        Assert.assertFalse(response.getBody().jsonPath().getString(tag).isEmpty());
    }

    @And("Verify the response body has private value {}")
    public void verifyTheResponseBodyHasPrivateValueValue(String value) {
        String privateValue = response.getBody().jsonPath().getString("private");
        Assert.assertEquals(value, privateValue);
    }

    @And("Verify the response body has message {}")
    public void verifyTheResponseBodyHasMessageMessage(String message) {
        String failureMessage = response.getBody().jsonPath().getString("message");
        Assert.assertEquals(message, failureMessage);
    }

    @When("User sends GET request to get bin using route {string}{}")
    public void userSendsGETRequestToGetBinUsingRouteBinID(String path, String binID, Map<String,String> binType) {
        if(binID.equals("true"))
        {
            binID = binids.get("true");
        } else if(binID.equals("false"))
        {
            binID = binids.get("false");
        }
        fullPath = path + "/" + binID;
        response = readBinsAPI.getBins(fullPath, binType);
    }

    @When("User sends UPDATE request to update bin using route {string}{}")
    public void userSendsUPDATERequestToUpdateBinUsingRouteBinID(String path, String binID, Map<String,String> updateAPI) {
        if(binID.equals("true"))
        {
            binID = binids.get("true");
        } else if(binID.equals("false"))
        {
            binID = binids.get("false");
        }

        fullPath = path + "/" + binID;
        response = updateBinsAPI.updateBins(fullPath, updateAPI);
    }

    @When("User sends UPDATE request without body to update bin using route {string}{}")
    public void userSendsUPDATERequestWithoutBodyToUpdateBinUsingRouteBinID(String path, String binID, Map<String,String> updateAPI) {
        if(binID.equals("true"))
        {
            binID = binids.get("true");
        } else if(binID.equals("false"))
        {
            binID = binids.get("false");
        }

        fullPath = path + "/" + binID;
        response = updateBinsAPI.updateBinsWithoutBody(fullPath, updateAPI);
    }

    @When("User sends DELETE request to get bin using route {string}{}")
    public void userSendsDELETERequestToGetBinUsingRouteBinID(String path, String binID, Map<String,String> deleteAPI) {
        if(binID.equals("true"))
        {
            binID = binids.get("true");
        } else if(binID.equals("false"))
        {
            binID = binids.get("false");
        }

        fullPath = path + "/" + binID;
        response = deleteBinsAPI.deleteBins(fullPath, deleteAPI);
    }

    @And("Verify the response body contains message {string}")
    public void verifyTheResponseBodyContainsMessageBinID(String message) {
        Assert.assertTrue(response.getBody().jsonPath().getString("message").contains(message));
    }

    @And("Save the binid and its type in a map")
    public void saveTheBinidAndItsTypeInAMap() {
        String binid = response.getBody().jsonPath().getString("id");
        String binType = response.getBody().jsonPath().getString("private");

        binids.put(binType, binid);
    }
}