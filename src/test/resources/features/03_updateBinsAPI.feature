@SmokeTest
Feature: Using the UPDATE API to update Public and Private bins.

##################### Positive Scenarios #####################
  Scenario Outline: Should update private and public bins using binID
    When User sends UPDATE request to update bin using route "/b"<binID>
      | contentType | <contentType> |
      | privateBin  | <privateBin>  |
      | versioning  | <versioning>  |

    Then Verify that response status code is <statusCode>
    And Verify that response content type is "application/json"
    And Verify the response schema is "schema/updateBinsAPI.json"
    And Verify the response header contains "Set-Cookie"
    And Verify the response header contains "ETag"
    And Verify the response header contains "CF-Cache-Status"
    And Verify the response header contains "cf-request-id"
    And Verify the response header contains "CF-RAY"
    And Verify the response body has success "true"
    And Verify the response body has tag "version"
    And Verify the response body has tag "parentId"

    Examples:
      | contentType      | binID | privateBin | versioning | statusCode |
      #passing valid private binID, secretkey = true and versioning = false to update a private bin
      | application/json | true  | true       | false      | 200        |
      #passing valid private binID, secretkey = true and versioning = true to update a private bin
      | application/json | true  | true       | true       | 200        |
      #passing valid public binID, secretkey = false and versioning = true to update a public bin
      | application/json | false | false      | true       | 200        |


##################### Negative Scenarios #####################
  Scenario Outline: Should update private and public bins using binID
    When User sends UPDATE request to update bin using route "/b"<binID>
      | contentType | <contentType> |
      | privateBin  | <privateBin>  |
      | versioning  | <versioning>  |

    Then Verify that response status code is <statusCode>
    And Verify that response content type is "application/json"
    And Verify the response header contains "Set-Cookie"
    And Verify the response header contains "ETag"
    And Verify the response header contains "CF-Cache-Status"
    And Verify the response header contains "cf-request-id"
    And Verify the response header contains "CF-RAY"
    And Verify the response body has success "false"
    And Verify the response body has message <message>

    Examples:
      | contentType      | binID                    | privateBin | versioning | statusCode | message                                             |
      #passing valid public binID, secretkey = false and versioning = false to update a public bin
      | application/json | false                    | false      | false      | 422        | You cannot Disable Versioning on Public Records.    |
      #passing invalid binID
      | application/json | 89tyigkvjb               | true       | false      | 422        | Invalid Bin ID                                      |
      #passing not found binID
      | application/json | 5f5d523dad23b57ef910ff60 | true       | false      | 404        | Bin not found                                       |
      #passing invalid secretkey to update private bin
      | application/json | true                     | invalid    | false      | 401        | Invalid secret key provided                         |
      #passing blank secretkey to update private bin
      | application/json | true                     | blank      | false      | 401        | Need to provide a secret-key to UPDATE private bins |
      #passing blank secretkey to update private bin
      | application/xml  | true                     | blank      | false      | 422        | Expected content type - application/json            |


##################### Specific Scenarios with not passing body #####################
  Scenario Outline: When not passing body
    When User sends UPDATE request without body to update bin using route "/b"<binID>
      | contentType | <contentType> |
      | privateBin  | <privateBin>  |
      | versioning  | <versioning>  |

    Then Verify that response status code is <statusCode>
    And Verify that response content type is "application/json"
    And Verify the response header contains "Set-Cookie"
    And Verify the response header contains "ETag"
    And Verify the response header contains "CF-Cache-Status"
    And Verify the response header contains "cf-request-id"
    And Verify the response header contains "CF-RAY"
    And Verify the response body has success "false"
    And Verify the response body has message <message>

    Examples:
      | contentType      | binID | privateBin | versioning | statusCode | message              |
      #passing valid public binID, secretkey = true and versioning = false to update a public bin
      | application/json | false | true       | false      | 422        | JSON cannot be empty |
