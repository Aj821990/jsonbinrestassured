@SmokeTest
Feature: Using the CREATE API to Create Public and Private bins.

##################### Positive Scenarios #####################
  Scenario Outline: Bin should be created using CREATE API
    When User sends POST request to create bin using route "/b"
      | contentType    | <contentType>    |
      | secretKey      | <secretKey>      |
      | collectionName | <collectionName> |
      | private        | <private>        |
      | binName        | <binName>        |

    Then Verify that response status code is <statusCode>
    And Verify that response content type is "application/json"
    And Verify the response schema is "schema/createBinsAPI.json"
    And Verify the response header contains "Set-Cookie"
    And Verify the response header contains "ETag"
    And Verify the response header contains "CF-Cache-Status"
    And Verify the response header contains "cf-request-id"
    And Verify the response header contains "CF-RAY"
    And Verify the response body has success "true"
    And Verify the response body has tag "id"
    And Verify the response body has private value <value>
    And Save the binid and its type in a map

    Examples:
      | contentType      | secretKey | collectionName | private | binName                                                                                                                          | statusCode | value |
      #passing all valid headers
      | application/json | valid     | collection1    | true    | bin1                                                                                                                             | 200        | true  |
      #passing mandatory headers (contentType, secretKey) and collectionName
      | application/json | valid     | collection1    |         |                                                                                                                                  | 200        | true  |
      #passing mandatory headers (contentType, secretKey) and collectionName and private
      | application/json | valid     | collection1    | true    |                                                                                                                                  | 200        | true  |
      #passing all headers and private=false which is creating a public bin
      | application/json | valid     | collection1    | false   |                                                                                                                                  | 200        | false |
      #passing all headers and private != true or false
      | application/json | valid     | collection1    | abcd    |                                                                                                                                  | 200        | true  |
      #passing all headers and bin name = 127 characters
      | application/json | valid     | collection1    | true    | binnameof127charactersbinnameof127charactersbinnameof127charactersbinnameof127charactersbinnameof127charactersbinnameof127chara  | 200        | true  |
      #passing all headers and bin name = 128 characters
      | application/json | valid     | collection1    | true    | binnameof128charactersbinnameof128charactersbinnameof128charactersbinnameof128charactersbinnameof128charactersbinnameof128charac | 200        | true  |


##################### Negative Scenarios #####################
  Scenario Outline: Bin should not be created using CREATE API when passing incorrect headers
    When User sends POST request to create bin using route "/b"
      | contentType    | <contentType>    |
      | secretKey      | <secretKey>      |
      | collectionName | <collectionName> |
      | private        | <private>        |
      | binName        | <binName>        |

    Then Verify that response status code is <statusCode>
    And Verify that response content type is "application/json"
    And Verify the response body has success "false"
    And Verify the response body has message <message>

    Examples:
      | contentType      | secretKey | collectionName | private | binName                                                                                                                           | statusCode | message                                                     |
      #passing secretKey = invalid
      | application/json | invalid   | collection1    | true    | bin1                                                                                                                              | 401        | Invalid secret key provided.                                |
      #passing secretKey = blank
      | application/json | blank     | collection1    | true    | bin1                                                                                                                              | 401        | You need to pass a secret-key in the header to Create a Bin |
      #passing invalid contentType
      | application/xml  | valid     | collection1    | true    | bin1                                                                                                                              | 422        | Expected content type - application/json                    |
      #passing collectionName = invalid
      | application/json | valid     | invalid        | true    | bin1                                                                                                                              | 422        | Invalid Collection ID                                       |
      #passing collectionName = blank
      | application/json | valid     |                | true    | bin1                                                                                                                              | 422        | Invalid Collection ID                                       |
      #passing binName = blank
      | application/json | valid     | collection1    | true    |                                                                                                                                   | 422        | Bin Name cannot be empty.                                   |
      #passing bin name = 129 characters
      | application/json | valid     | collection1    | abcd    | binnameof129charactersbinnameof129charactersbinnameof129charactersbinnameof129charactersbinnameof129charactersbinnameof129charact | 422        | Bin Name cannot be longer than 128 characters.               |


##################### Specific Scenarios with only mandatory headers #####################
  Scenario Outline: When not passing optional headers
    When User sends POST request to create bin using only mandatory headers route "/b"
      | contentType | <contentType> |
      | secretKey   | <secretKey>   |

    Then Verify that response status code is <statusCode>
    And Verify that response content type is "application/json"
    And Verify the response schema is "schema/createBinsAPI.json"
    And Verify the response header contains "Set-Cookie"
    And Verify the response header contains "ETag"
    And Verify the response header contains "CF-Cache-Status"
    And Verify the response header contains "cf-request-id"
    And Verify the response header contains "CF-RAY"
    And Verify the response body has success "true"
    And Verify the response body has tag "id"
    And Verify the response body has private value <value>

    Examples:
      | contentType      | secretKey | statusCode | value |
      | application/json | invalid   | 200        | true  |


##################### Specific Scenarios with not passing body #####################
  Scenario Outline: When not passing body
    When User sends POST request to create bin using without body route "/b"
      | contentType | <contentType> |
      | secretKey   | <secretKey>   |

    Then Verify that response status code is <statusCode>
    And Verify the response body has success "false"
    And Verify the response body has message <message>

    Examples:
      | contentType      | secretKey | statusCode | message              |
      | application/json | invalid   | 422        | JSON cannot be empty |
