@SmokeTest
Feature: Using the READ API to Get Public and Private bins.

##################### Positive Scenarios #####################
  Scenario Outline: Should get private and public bins using binID
    When User sends GET request to get bin using route "/b"<binID>
      | privateBin | <privateBin> |

    Then Verify that response status code is <statusCode>
    And Verify that response content type is "application/json"
    And Verify the response schema is "schema/readBinsAPI.json"
    And Verify the response header contains "Set-Cookie"
    And Verify the response header contains "ETag"
    And Verify the response header contains "CF-Cache-Status"
    And Verify the response header contains "cf-request-id"
    And Verify the response header contains "CF-RAY"

    Examples:
      | binID | privateBin | statusCode |
      #passing valid private binID and secretkey = true to access a private bin
      | true  | true       | 200        |
      #passing valid public binID and secretkey = false to access a public bin
      | false  | false      | 200        |
      #passing invalid secretkey to get public bin
      | false | invalid    | 200        |
      #passing blank secretkey to get public bin
      | false | blank      | 200        |


##################### Negative Scenarios #####################
  Scenario Outline: Should not get private and public bins using binID
    When User sends GET request to get bin using route "/b"<binID>
      | privateBin | <privateBin> |

    Then Verify that response status code is <statusCode>
    And Verify that response content type is "application/json"
    And Verify the response header contains "Set-Cookie"
    And Verify the response header contains "ETag"
    And Verify the response header contains "CF-Cache-Status"
    And Verify the response header contains "cf-request-id"
    And Verify the response header contains "CF-RAY"
    And Verify the response body has success "false"
    And Verify the response body has message <message>

    Examples:
      | binID                    | privateBin | statusCode | message                                           |
      #passing invalid binID
      | 89tyigkvjb               | true       | 422        | Invalid Bin ID                                    |
      #passing not found binID
      | 5f5d523dad23b57ef910ff60 | true       | 404        | Bin not found                                     |
      #passing invalid secretkey to get private bin
      | true                     | invalid    | 401        | Invalid secret key provided.                      |
      #passing blank secretkey to get private bin
      | true                     | blank      | 401        | Need to provide a secret-key to READ private bins |
