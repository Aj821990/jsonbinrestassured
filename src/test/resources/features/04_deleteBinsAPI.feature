@SmokeTest
Feature: Using the DELETE API to delete Public and Private bins.

##################### Negative Scenarios #####################
  Scenario Outline: Should delete private and public bins using binID
    When User sends DELETE request to get bin using route "/b"<binID>
      | privateBin | <privateBin> |

    Then Verify that response status code is <statusCode>
    And Verify that response content type is "application/json"
    And Verify the response schema is "schema/deleteBinsAPI.json"
    And Verify the response header contains "Set-Cookie"
    And Verify the response header contains "ETag"
    And Verify the response header contains "CF-Cache-Status"
    And Verify the response header contains "cf-request-id"
    And Verify the response header contains "CF-RAY"
    And Verify the response body has success "false"
    And Verify the response body has message <message>

    Examples:
      | binID                    | privateBin | statusCode | message                                     |
      #passing valid private binID without secret key to delete a private bin
      | true                     | false      | 401        | Need to provide a secret-key to DELETE bins |
      #passing valid public binID with invalid secret key to delete a public bin
      | false                    | invalid    | 401        | Invalid secret key provided                 |
      #passing valid public binID with blank secret key to delete a public bin
      | false                    | blank      | 401        | Need to provide a secret-key to DELETE bins |
      #passing invalid binID to delete a public bin
      | 89tyigkvjb               | true       | 422        | Invalid Bin ID                              |
      #passing already deleted binID to delete a bin
      | 5f5e4415302a837e95657b5d | true       | 404        | Bin not found                               |


##################### Positive Scenarios #####################
  Scenario Outline: Should delete private and public bins using binID
    When User sends DELETE request to get bin using route "/b"<binID>
      | privateBin | <privateBin> |

    Then Verify that response status code is <statusCode>
    And Verify that response content type is "application/json"
    And Verify the response schema is "schema/deleteBinsAPI.json"
    And Verify the response header contains "Set-Cookie"
    And Verify the response header contains "ETag"
    And Verify the response header contains "CF-Cache-Status"
    And Verify the response header contains "cf-request-id"
    And Verify the response header contains "CF-RAY"
    And Verify the response body has success "true"
    And Verify the response body has tag "id"
    And Verify the response body contains message " is deleted successfully."

    Examples:
      | binID | privateBin | statusCode |
      #passing valid private binID to delete a private bin
      | true  | true       | 200        |
      #passing valid public binID to delete a public bin
      | false | true       | 200        |

