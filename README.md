# JsonBinRestAssured

API Automation with Selenium Rest-Assured, Java8, Maven, Serenity BDD Framework and gitlab-ci

## Requirements:
Below dependencies needs to be installed/configured
- Java 8 or higher (JAVA_HOME and PATH in environmental variables)
- Maven (M2, MAVEN_HOME and PATH in environmental variables)
- IDE (IntelliJ is preferred)
- It would be nice to have "Cucumber for Java" and "Gherkin" plugins installed in your IDE for better understanding and working of cucumber

## Downloading Project:
- Using git command and clone with HTTPS:
~~~
https://gitlab.com/Aj821990/jsonbinrestassured.git
~~~
*or*
- Using git command and SSH key configured in your system:
~~~
git@gitlab.com:Aj821990/jsonbinrestassured.git
~~~
*or*
- Directly downloading the project as zip file from
~~~
https://gitlab.com/Aj821990/jsonbinrestassured
~~~

##Creating new feature file
- Existing feature files are currently located at "src/test/resources/features"
- If you want to create a new feature file, you can go the specified folder and create a new feature file with the naming convention: <orderOfExecution>_<featureFileName>.feature
    Example: If you need to create a new feature file with delete operation, then the naming convention is 05_deleteAPI.feature
- If you want to edit existing feature file, you can select the feature file which needs to be edited in the specified folder structure
- In case you do not want to pass any data values in feature file, please create a feature file with "Scenario" instead of "Scenario Outline" and remove the "Examples" section
- All the steps you write should be in Gherkin language (for reference see existing feature files)
- When you start typing your steps, you will get options to reuse the steps. You can choose to select the existing step or create a new step based on your scenario to be tested

##Creating new step definition
- Once you create a (new) step in feature file, you will see your steps being highlighted as you have not created any step definition for your steps in feature file
- You can select where you want your step definition to be written (by default we choose the same step definition file to reduce rework and complexities)
- In this project, as there is only one step definition file at location "src/test/java/framework/step_definitions", you can either choose to create your step definition there or create a new file
- In-case you choose to create a new step definition file, make sure you create it in the same package as that of the existing step definition file i.e. src/test/java/framework/step_definitions
- Once you have created your step defintion, give the logic you want the step definition to do

## Execution:

In-case of errors, please check/update your dependencies in pom.xml file based on your IDE and System configurations.

1.. Run via runner file
````sh
Right click on the runner file and select Run
Example:
Runner file in this project is CukesRunnerTest. Right click and select -> Run 'CukesRunnerTest'
````
2.. Run via terminal
```sh
mvn verify
```
3.. Run via gitlab CI pipeline
```sh
All commands are configure in .gitlab-ci.yml file. So you can directly run the project via gitlab ci pipeline
```

##Important points to remember:
- ***Scenarios covered:*** Scenarios only with the header "Content-Type: application/json" is covered
- ***Reporting:*** Serenity BDD framework default reporting is used in this project
- ***Test Report:*** Test Report is saved in location - "target/site/serenity/index.html".
                     Test report will be visible locally only after executing the project from your terminal with command "mvn verify".
                     ****It is adviced to run mvn clean first before running mvn verify****
- ***.gitignore:*** .idea/, target/ and .iml are covered in .gitignore file
- ***Step Definitions:*** There is only single step definition file configured in this project as the services to be covered are less.
                          We can split the step definitions in four different files as per services (example: post, get, update and delete)
- ***CI pipeline:*** The gitlab ci pipeline will fail as the scenarios are failing.
- ***Artifacts:*** Artifacts after executing the pipeline can be downloaded from gitlab. The HTML report will not be in great shape as we are just sharing the final HTML report and not the entire package.
- ***Service Interactions:*** End to end flow is not covered yet. This project currently covers functional testing of individual services.
- ***Unused class/files:*** The files docker-compose.yml, log4j.properties, DBConnection.class, Queries.class, .circleci folder are mentioned for knowledge purpose only and not used anywhere in the application

Please get in touch with me at *antojohn.8@gmail.com* for any queries regarding this project